Ubuntu minimal
==============

Minimal Ubuntu docker image.

Size around 100 MB.

Created using:

    debootstrap --variant=minbase trusty root/ http://archive.ubuntu.com/ubuntu

Contents
--------

  * bootstrap.sh - create root filesystem using debootstrap, run *dist-upgrade*
  * create_image.sh - creates docker image from the root filesystem

Locations
---------

Scripts for creating the image are hosted in a Bitbucket repository
https://bitbucket.org/beli-sk/docker-ubuntu-minimal

Please report any problems in the issue tracker at
https://bitbucket.org/beli-sk/docker-ubuntu-minimal/issues

The compiled image is available for pull at Docker Hub
https://hub.docker.com/r/beli/ubuntu-minimal/

