#!/bin/bash

cd root/
sudo tar -c . | docker import \
	-c 'WORKDIR /root' \
	-c 'ENV HOME /root' \
	-c 'ENV USER root' \
	-c 'CMD ["/bin/bash"]' \
	- beli/ubuntu-minimal
