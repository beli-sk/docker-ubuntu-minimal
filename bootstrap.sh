#!/bin/bash

ROOTDIR='root'

[[ -z "$ROOTDIR" || "$ROOTDIR" == '/' ]] && exit 1

debootstrap --variant=minbase trusty "${ROOTDIR}" http://archive.ubuntu.com/ubuntu
cp sources.list "${ROOTDIR}/etc/apt/"
pushd "${ROOTDIR}"
cat > usr/sbin/policy-rc.d <<EOF
#!/bin/sh
exit 101
EOF
chmod a+x usr/sbin/policy-rc.d
chroot . bin/bash -c 'apt-get update ; apt-get dist-upgrade -y ; apt-get autoremove --purge -y'
rm usr/sbin/policy-rc.d
popd

echo '*** cleanup'
truncate -s0 "${ROOTDIR}/etc/resolv.conf"
echo 'localhost' > "${ROOTDIR}/etc/hostname"
find "${ROOTDIR}"/var/cache/apt/ -type f -delete
find "${ROOTDIR}"/var/lib/apt/lists/ -type f -delete
rm "${ROOTDIR}"/etc/{passwd,group,shadow,subuid,subgid}-

